/*
 * Copyright 2018, The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.example.android.trackmysleepquality.sleeptracker

import android.app.Application
import android.provider.SyncStateContract.Helpers.insert
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import com.example.android.trackmysleepquality.database.SleepDatabaseDao
import com.example.android.trackmysleepquality.database.SleepNight
import com.example.android.trackmysleepquality.formatNights
import kotlinx.coroutines.*

/**
 * ViewModel for SleepTrackerFragment.
 */
class SleepTrackerViewModel(
        val database: SleepDatabaseDao,
        application: Application) : AndroidViewModel(application) {

    // create view model job and cancel job on view model clear
    private var viewModelJob = Job()
    override fun onCleared() {
        super.onCleared()
        viewModelJob.cancel()
    }

    // job scope
    private val uiScope = CoroutineScope(Dispatchers.Main + viewModelJob)

    // create a live data variable for tonight's sleep
    private var tonight = MutableLiveData<SleepNight?>()

    // get all sleep data
    val nights = database.getAllNights()
    // transform the nights list into a usable string
    // formatNights is in the utilities file
    val nightsString = Transformations.map(nights) { nights -> formatNights(nights, application.resources) }

    // button visibility

    // start button is visible when no recording has started for sleep tonight (tonight is null)
    val startButtonVisible = Transformations.map(tonight) { null == it }
    // stop button is visible when recording has started for sleep tonight (tonight is not null)
    val stopButtonVisible = Transformations.map(tonight) { null != it }
    // clear button is visible when there are entries in the database (nights List returned from the database is not empty)
    val clearButtonVisible = Transformations.map(nights) { it?.isNotEmpty() }

    // snackbar
    private var _showSnackbar = MutableLiveData<Boolean>()
    val showSnackbar: LiveData<Boolean> get() = _showSnackbar
    fun hideSnackbar() { _showSnackbar.value = false }

    // navigation to sleep quality fragment passing a SleepNight object
    private val _navigateToSleepQuality = MutableLiveData<SleepNight>()
    val navigateToSleepQuality: LiveData<SleepNight> get() = _navigateToSleepQuality

    fun doneNavigating() { _navigateToSleepQuality.value = null }

    // navigation to sleep quality data fragment passing nightId
    private val _navigateToSleepDataQuality = MutableLiveData<Long>()
    val navigateToSleepDataQuality get() = _navigateToSleepDataQuality

    fun onSleepNightClicked(id: Long) { _navigateToSleepDataQuality.value = id }
    fun onSleepDataQualityNavigated() { _navigateToSleepDataQuality.value = null }


    // initialize tonight variable
    init {
        initializeTonight()
    }

    // initializing tonight variable uses a coroutine
    private fun initializeTonight() {
        uiScope.launch { tonight.value = getTonightFromDatabase() }
    }

    // database query happens on another coroutine on the IO thread
    private suspend fun getTonightFromDatabase(): SleepNight? {
        return withContext(Dispatchers.IO) {
            var night = database.getTonight()
            if (night?.endTimeMilli != night?.startTimeMilli) night = null
            night
        }
    }

    // click handler function for start tracking which uses a coroutine
    fun onStartTracking() {
        uiScope.launch {
            val newNight = SleepNight()
            insert(newNight)
            tonight.value = getTonightFromDatabase()
        }
    }

    // database insertion happens on another coroutine on the IO thread using a suspend function
    private suspend fun insert(newNight: SleepNight) {
        withContext(Dispatchers.IO) { database.insert(newNight) }
    }

    // click handler function for stopping tracking which happens on a coroutine
    fun onStopTracking() {
        uiScope.launch {
            val oldNight = tonight.value ?: return@launch
            oldNight.endTimeMilli = System.currentTimeMillis()
            update(oldNight)
            _navigateToSleepQuality.value = oldNight
        }
    }

    // database insertion happens on another coroutine on the IO thread using a suspend function
    private suspend fun update(oldNight: SleepNight) {
        withContext(Dispatchers.IO) { database.update(oldNight) }
    }

    // click handler function to clear all entries from the database
    fun onClear() {
        uiScope.launch {
            clear()
            tonight.value = null
            _showSnackbar.value = true
        }
    }

    // delete all sleep night data
    private suspend fun clear() {
        withContext(Dispatchers.IO) { database.clear() }
    }
}

